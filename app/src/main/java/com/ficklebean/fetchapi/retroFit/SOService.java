package com.ficklebean.fetchapi.retroFit;


import com.ficklebean.fetchapi.model.GetResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by MY PC on 6/16/2017.
 */

public interface SOService {


    @GET("read.php")
    Call<GetResponse> getNews();


}
