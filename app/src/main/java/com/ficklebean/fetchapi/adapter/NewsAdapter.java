package com.ficklebean.fetchapi.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ficklebean.fetchapi.R;
import com.ficklebean.fetchapi.model.News;

import java.util.List;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private List<News> mItems;
    private Context mContext;
    private PostItemListener mItemListener;

    public NewsAdapter(Context context, List<News> posts, PostItemListener itemListener) {
        mItems = posts;
        mContext = context;
        mItemListener = itemListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View postView = inflater.inflate(R.layout.news_card, parent, false);

        ViewHolder viewHolder = new ViewHolder(postView, this.mItemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        News item = mItems.get(position);
        TextView news = holder.news;
        TextView id = holder.id;
        TextView content = holder.content;
        ImageView newsImage = holder.newsImage;
        news.setText(item.getName());

        id.setText(item.getId());
        content.setText(item.getDescription());

        Glide.with(mContext).load(item.getImage())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(newsImage);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateAnswers(List<News> items) {

            mItems = items;
            notifyDataSetChanged();



    }

    private News getItem(int adapterPosition) {
        return mItems.get(adapterPosition);
    }

    public interface PostItemListener {
        void onPostClick(String id);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView news, id, content;
        ImageView newsImage;
        PostItemListener mItemListener;

        public ViewHolder(View itemView, PostItemListener postItemListener) {
            super(itemView);
            news = (TextView) itemView.findViewById(R.id.news);
            id = (TextView) itemView.findViewById(R.id.id);
            newsImage = (ImageView) itemView.findViewById(R.id.image);
            content = (TextView) itemView.findViewById(R.id.desciption);

            this.mItemListener = postItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            News item = getItem(getAdapterPosition());
            //   this.mItemListener.onPostClick(item.getId());
            notifyDataSetChanged();
        }
    }
}
